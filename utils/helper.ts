const maskLengthList = [256, 128, 64, 32, 16, 8, 4, 2];

export const inRange = (number:number, min:number, max:number) => number >= min && number <= max;

export const partOfRegex = (part:number, netmask:number) => {
  const mIndex = netmask % 8;
  if (mIndex === 0 && netmask === 0) return '(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)';
  const maskLength = maskLengthList[mIndex];
  const maskIndex = Math.floor(part / maskLength);
  const startN = Array.from((maskIndex * maskLength).toString().padStart(3, '0')).map((e)=>parseInt(e, 10));
  const endN = Array.from((((maskIndex + 1) * maskLength) - 1).toString().padStart(3, '0')).map((e)=>parseInt(e, 10));
  const regArray = [];
  const fillQuestionMark = (hunDigit:string, tenDigit:string, digit:string) => (
    `${hunDigit}${hunDigit === '[0]' ? '?' : ''}` +
    `${tenDigit}${tenDigit === '[0]' ? '?' : ''}` +
    `${digit}${digit === '[0]' ? '?' : ''}`
  );

  // ip區間開頭 000-009 | 004-007 等
  regArray.push(
      fillQuestionMark(`[${startN[0]}]`,
          `[${startN[1]}]`,
          `[${startN[2]}-${startN[1] === endN[1] ? endN[2] : '9'}]`),
  );
  // ip區間中段 010-999 大部分皆為9結尾
  if (startN[0] !== endN[0] || endN[1] - startN[1] > 1) {
    if (startN[1] !== 9) { // ip開頭為 00x-08x | 10x-18x 不會導致百位數進位
      regArray.push(
          fillQuestionMark(`[${startN[0] + startN[1] === 9 ? '1' : '0'}]`,
              `[${(startN[1] + 1) % 10}-${endN[0] > startN[0] ? '9' : (endN[1] - 1)}]`,
              '[0-9]'),
      );
      // 如果起始到結束有過一個百位數要再加進去 ex: startN = 000, endN = 127; 前面的只會有 010-099, 加上下段會有 100-110
      if (startN[0] !== endN[0]) {
        regArray.push(
            fillQuestionMark(`[${startN[0] + 1}]`,
                `[0-${endN[1] - 1}]`,
                '[0-9]'),
        );
      }
    } else if (endN[1] !== 0) { // ip開頭為 09x | 19x
      regArray.push(
          fillQuestionMark(`[${endN[0]}]`,
              `[0-${endN[1] - 1}]`,
              '[0-9]'),
      );
    }
  }
  // ip區間尾段 200-255 or 200-204 or 128-254 等
  regArray.push(
      fillQuestionMark(`[${endN[0]}]`,
          `[${startN[0] === endN[0] ? startN[1] + (startN[1] === endN[1] ? '0' : '1') : '0'}-${endN[1]}]`,
          `[${startN[0] === endN[0] && startN[1] === endN[1] ? startN[2] : '0'}-${endN[2]}]`),
  );
  return `(${regArray.join('|')})`;
};

