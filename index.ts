import {inRange, partOfRegex} from './utils/helper';

/**
 * Convert IP to Number Array
 * @param {string} ip IP address(allow include subnet number)
 * @return {number[]|null} IP Array(length is always 4) or null
 */
export const IPtoArray = (ip:string) => {
  const ipArray = ip.split('/')[0].split('.').map((e) => parseInt(e, 10));
  if (ipArray.length !== 4) return null;
  for (const v of ipArray) if (!inRange(v, 0, 255)) return null;
  return ipArray;
};

/**
 * Get subnet mask string.
 ** Example subnet number is 24 => 255.255.255.0
 * @param {number} subnetNum subnet number(0-32)
 * @return {string|null} netmask 0.0.0.0-255.255.255.255
 */
export const getSubnetMaskString = (subnetNum:number) => {
  if (!inRange(subnetNum, 0, 32)) return null;

  let total = 0;
  let powTimes = 7;
  let doTimes = subnetNum % 8;
  while (doTimes > 0) {
    total += 2 ** powTimes--;
    doTimes --;
  }
  const val = total;

  return (()=>{
    if (subnetNum === 32) return [255, 255, 255, 255];
    if (subnetNum >= 24) return [255, 255, 255, val];
    if (subnetNum >= 16) return [255, 255, val, 0];
    if (subnetNum >= 8) return [255, val, 0, 0];
    if (subnetNum >= 0) return [val, 0, 0, 0];
    return [0, 0, 0, 0];
  })().join('.');
};

/**
 * Get subnet mask regular expression.
 ** Example ip address is 192.168.0.1 subnet number is 24
 ** Result ^192\.168\.0\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$
 * @param {string} ip IP Address
 * @param {number} netmask subnet number(0-32)
 * @return {string|null} netmask 0.0.0.0-255.255.255.255
 */
export const getSubnetMaskRegex = (ip:string, netmask:number) => {
  if (typeof netmask === 'string') netmask = parseInt(netmask, 10);
  const ipArray = IPtoArray(ip);
  const netmaskArray = IPtoArray(getSubnetMaskString(netmask)!);
  if (!ipArray || !netmaskArray) return null;
  const ipRegexArray:string[] = [];
  ipArray.forEach((ipPart, i) => {
    const max = i * 8 + 8;
    const min = i * 8;
    if (netmask >= max) ipRegexArray.push(`${ipPart}`);
    else if (netmask <= min) ipRegexArray.push(partOfRegex(0, 0));
    else ipRegexArray.push(partOfRegex(ipPart, netmask));
  });

  return `^${ipRegexArray.join('\\.')}$`;
};

/**
 * Get subnet number by full subnet string
 * @param {string} ip IP Address
 * @return {number|null} subnet number
 */
export const getSubnetNumber = (ip:string) => {
  const last = ip.split('/').pop();
  if (!last) return 32;
  const mask = parseInt(last, 10);
  if (mask >= 0 && mask <= 32) return mask;
  return 32;
};

/**
 * Check `targetIP` is in `ipWithMask` subnet
 * @param {string} targetIP
 * @param {string} ipWithMask
 * @return {boolean}
 */
export const IPinSubnet = (targetIP:string, ipWithMask:string) => {
  const targetIPArray = IPtoArray(targetIP);
  const fromIPArray = IPtoArray(ipWithMask);
  const fromIPMask = getSubnetNumber(ipWithMask);
  if (!targetIPArray) return false;
  if (!fromIPArray) return false;
  if (!fromIPMask) return false;
  if (!inRange(fromIPMask, 0, 32)) return false;
  const ipRegex = getSubnetMaskRegex(fromIPArray.join('.'), fromIPMask);
  if (!ipRegex) return false;
  return !!targetIPArray.join('.').match(new RegExp(ipRegex));
};

/**
 * Get IP Privacy
 * @param {string} ip IP Address
 * @return {string} IP privacy
 */
export const getIPPrivacy = (ip:string) => {
  const ipArray = IPtoArray(ip);
  if (!ipArray || ipArray.length !== 4) return null;
  if (ip === '127.0.0.1') return 'localhost';
  if (IPinSubnet(ip, '10.0.0.0/8')) return 'Private IP';
  if (IPinSubnet(ip, '172.16.0.0/12')) return 'Private IP';
  if (IPinSubnet(ip, '192.168.0.0/16')) return 'Private IP';
  return 'Public IP';
};

/**
 * Get IP Class
 * @param {string} ip IP Address
 * @return {string} IP Class
 */
export const getIPClass = (ip:string) => {
  const ipArray = IPtoArray(ip);
  if (!ipArray) return null;
  const num = ipArray[0];
  if (num <= 127) return 'A';
  if (num <= 191) return 'B';
  if (num <= 223) return 'C';
  if (num <= 239) return 'D';
  if (num <= 255) return 'E';
};

export const getIPInfo = (ip:string, netmask:number = 32) => {
  const ipArray = IPtoArray(ip);
  const netmaskArray = IPtoArray(getSubnetMaskString(netmask)!);
  return {
    address: ipArray?.length === 4 ? ipArray.join('.') : 'invalid',
    netmask: netmaskArray ? netmaskArray.join('.') : 'invalid',
    class: getIPClass(ip) || 'unknown',
    privacy: getIPPrivacy(ip) || 'unknown',
  };
};

module.exports = {
  IPtoArray,
  getSubnetMaskString,
  getSubnetMaskRegex,
  getSubnetNumber,
  IPinSubnet,
  getIPPrivacy,
  getIPClass,
  getIPInfo,
};
