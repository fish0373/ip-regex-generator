import {getSubnetMaskRegex} from '..';

console.log(getSubnetMaskRegex('192.168.0.1', 24));
// => ^192\.168\.0\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$

console.log(getSubnetMaskRegex('192.168.0.1', 26));
// => ^192\.168\.1\.([0]?[0]?[0-9]|[0]?[1-5][0-9]|[0]?[1-6][0-3])$

// @ts-nocheck
console.log(getSubnetMaskRegex('192.168.0.1', 8));
// => ^192\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$

console.log(getSubnetMaskRegex('192.168.0', 8)); // incomplete
console.log(getSubnetMaskRegex('192.168.0.*', 8)); // not support
// => null
