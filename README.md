# ip-regex-generator

> Generate valid subnet ip regex string by ip in netmask.


## Install

```
$ npm install ip-regex-generator
```


## Usage

```js
const { getSubnetMaskRegex } = require('ip-regex-generator');

getSubnetMaskRegex('192.168.0.1', 24);
getSubnetMaskRegex('192.168.0.1', 24);
//=> ^192\.168\.0\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$

getSubnetMaskRegex('192.168.0.1', 26);
//=> ^192\.168\.1\.([0]?[0]?[0-9]|[0]?[1-5][0-9]|[0]?[1-6][0-3])$

getSubnetMaskRegex('192.168.0.1', 8);
//=> ^192\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$

getSubnetMaskRegex('192.168.0', 8); // incomplete
getSubnetMaskRegex('192.168.0.*', 8); // not support
//=> null
```


## Function

### getIPArray(`string` ip)
Get IP `Array` from IP `string`.<br/>
If ip is invalid return `null`.
```js
IPtoArray('192.168.0.1');
//=> [192, 168, 0, 1]

IPtoArray('192.168.');
//=> null
```

### getSubnetMaskString(`number` netmask)
*netmask value allow `0-32`<br/>
Get IP type netmask `string` from netmask `number`<br/>
If netmask invalid return `null`.
```js
getSubnetMaskString(24);
//=> 255.255.255.0

getSubnetMaskString(8);
//=> 255.0.0.0

getSubnetMaskString(33);
//=> null
```

### getSubnetNumber(`string` ip)

Get subnetmask `number` by network.<br/>
If network not have netmask number or number is invalid return `null`.

```js
getSubnetNumber('192.168.0.1/24');
//=> 24

getSubnetNumber('192.168.0.1');
//=> 32

getSubnetNumber('192.168.0.1/33');
//=> null
```

### IPinSubnet(`string` targetIP, `string` ipWithMask)
Check `targetIP` is in `ipWithMask` subnet
```js
IPinSubnet('192.168.1.254', '192.168.0.0/24')
//=> true
IPinSubnet('192.168.3.1', '192.168.0.0/24')
//=> false
IPinSubnet('192.168.3.1', '192.168.0.0/16')
//=> true
IPinSubnet('192.168.3', '192.168.0.0/24')
//=> null
IPinSubnet('192.168.3.1', '192.168.0.0/33')
//=> null
```

### getIPPrivacy(`string` ip)
Get IP Privacy.
```js
getIPPrivacy('192.168.0.1')
//=> Private IP

getIPPrivacy('127.0.0.1')
//=> localhost

getIPPrivacy('8.8.8.8')
//=> Public IP

getIPPrivacy('192.168.0')
//=> null
```

### getIPClass(`string` ip)

Get subnet class(`A, B, C, D, E`) by ip.<br />
If ip is invalid return `null`.
```js
getIPClass('8.8.8.8');
//=> A

getIPClass('192.168.0.1');
//=> C

getIPClass('192.168')
getIPClass('300.300.0.1')
//=> null
```

### getIPInfo(`string` ip, `number` netmask)
`*netmask` is optional<br />
Get IP Information.
```js
getIPInfo('192.168.0.1', 24)
//=> IP Address: 192.168.0.1
//   IP Class: C
//   Netmask: 255.255.255.0
//   Subnet Type: Private IP

getIPInfo('192.168.0.1')
//=> IP Address: 192.168.0.1
//   IP Class: C
//   Netmask: 255.255.255.255
//   Subnet Type: Private IP

getIPInfo('192.168.0', 24)
// IP Address: invalid
// Netmask: 255.255.255.0
// IP Class: Unknown
// Subnet Type: Unknown
```


## License

MIT © [Rong Wu](https://gitlab.com/fish0373)